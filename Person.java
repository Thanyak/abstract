package bcas.ap.abs;

public abstract class Person {
	
	private String name;
	private String gender;
	
	public Person(String name, String gender) {
		this.name=name;
		this.gender=gender;
	}

	//abstract method
	public abstract void work();

	public String toString() {
		return "Name : "+ this.name+"\r\n"+"Gender : "+this.gender;
		
	}

	public void changeName(String newName) {
		this.name=newName;
	}

}
