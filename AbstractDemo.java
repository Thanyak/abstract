package bcas.ap.abs;

public class AbstractDemo {

	public static void main(String[] args) {
		//coding in terms of abstract classes
		
		Person student = new Employee("Thanu","Female",0);
		Person employee= new Employee("Kajan","Male",123);
		
		student.work();
		employee.work();
		
		// Using method implemented in abstract class-inheritance
		employee.changeName("Kajananth");
		System.out.println(employee.toString());

	}

}
