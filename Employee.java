package bcas.ap.abs;

public class Employee extends Person{
	
	private int emId;
	
	public Employee(String name,String gender,int emId) {
		super(name, gender);
		this.emId=emId;
	
	}
	
	@Override
	public void work() {
		// TODO Auto-generated method stub
		if (emId==0) {
			System.out.println("Not working");
		}
		else {
			System.out.println("Working as employee!!");
		}
	}

	}

